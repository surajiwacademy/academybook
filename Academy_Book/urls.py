from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns

from django.contrib import admin
from django.urls import path, re_path, include

from accounts.views import home
from hashTags.views import HashTagView

urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
    path('admin/', admin.site.urls),
    path('api/', include('analytics.urls')),
    path('accounts/', include('accounts.urls', namespace="profiles")),
    path('api/', include('accounts.api.urls', namespace="profiles-api")),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/', include('allauth.urls')),
    path('calendar/', include('cal.urls')),
    path('chat/', include('chat.urls', namespace='chat')),
    path('posts/', include('posts.urls', namespace='posts')),
    path('api/posts/', include('posts.api.urls', namespace='posts-api')),
    path('', home, name='home'),
    re_path(r'^tags/(?P<hashtag>.*)/$', HashTagView.as_view(), name='hashtag'),
    path('api/', include('comments.api.urls', namespace='comment')),

]

# urlpatterns += i18n_patterns(
#     path('admin/', admin.site.urls),
#     path('api/', include('analytics.urls')),
#     path('accounts/', include('accounts.urls', namespace="profiles")),
#     path('api/', include('accounts.api.urls', namespace="profiles-api")),
#     path('accounts/', include('django.contrib.auth.urls')),
#     path('accounts/', include('allauth.urls')),
#     path('posts/', include('posts.urls', namespace='posts')),
#     path('api/posts/', include('posts.api.urls', namespace='posts-api')),
#     path('', home, name='home'),
#     re_path(r'^tags/(?P<hashtag>.*)/$', HashTagView.as_view(), name='hashtag'),
#     path('api/', include('comments.api.urls', namespace='comment')),
#     prefix_default_language=False
#
# )

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
