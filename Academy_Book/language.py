from django.utils.translation import gettext as _


class Translate(object):
    def get_context_data(self, *args, **kwargs):
        context = super(Translate, self).get_context_data(*args, **kwargs)
        title = _('Homepage')
        notify = _("Notifications")
        profile = _("Profile")
        settings = _("Settings")
        search = _("Search")
        context['title'] = title
        context['notify'] = notify
        context['profile'] = profile
        context['settings'] = settings
        context['search'] = search

        return context
