from django.urls import re_path

from .views import ChartData, HomeView, get_data

urlpatterns = [
    re_path(r'^home/$', HomeView.as_view(), name='home'),
    re_path(r'^data/$', get_data, name='api-data'),
    re_path(r'^chart/data/$', ChartData.as_view()),

]
