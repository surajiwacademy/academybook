from django.contrib.auth import get_user_model
from django.db.models import Count, Sum
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.views.generic import View

from rest_framework.views import APIView
from rest_framework.response import Response

from analytics.models import ObjectViewed
from posts.models import Post

User = get_user_model()


class HomeView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'chart.html', {"customers": 10})


def get_data(request, *args, **kwargs):
    data = {-
        "sales": 100,
        "customers": 10,
    }
    return JsonResponse(data)  # http response


class ChartData(APIView):

    def get(self, request):
        # u = User.objects.values_list('username', flat=True)
        # content = Post.objects.all().annotate(num_post=Count('post')).filter('user__username')
        labels = []
        default_items = []

        qs = Post.objects.values('user__username').annotate(post=Sum('user_id')).order_by('user__username')
        for count in qs:

            labels.append(count['user__username'])
            default_items.append(count['post'])

        labels = labels
        default_items = default_items
        data = {
            "labels": labels,
            "default": default_items,
        }
        print(data)
        return Response(data)
