from django.urls import path

from .views import CommentDetailAPIView, CommentListAPIView, CreateCommentAPIView, EditCommentAPIView

app_name = 'comment'

urlpatterns = [
    path('comment-detail/<int:pk>/', CommentDetailAPIView.as_view(), name="comment-detail"),
    path('comment-list/', CommentListAPIView.as_view(), name="comment-list"),
    path('create_comment/', CreateCommentAPIView.as_view(), name="create-comment"),
    path('edit_comment/<int:pk>/', EditCommentAPIView.as_view(), name="edit-comment")
]
