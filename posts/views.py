from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from django.db.models import Q
from django.utils import translation
from urllib.parse import quote_plus
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.translation import gettext as _
from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView

from django.contrib.auth import get_user_model

from Academy_Book.language import Translate
from accounts.models import Customer
from comments.models import Comment
from .mixins import FormUserNeededMixin, UserOwnerMixin, Paid
from analytics.mixins import ObjectViewedMixin
from django.views import View
from django.urls import reverse_lazy
from .models import Post

from .forms import PostModelForm

User = get_user_model()


class RePostView(View):
    def get(self, request, pk, *args, **kwargs):
        post = get_object_or_404(Post, pk=pk)
        if request.user.is_authenticated:
            new_post = Post.objects.repost(request.user, post)
            return HttpResponseRedirect('/posts/post-list/')
        return HttpResponseRedirect(post.get_absolute_url())


class PostCreateView(LoginRequiredMixin, FormUserNeededMixin, CreateView):
    form_class = PostModelForm
    template_name = 'posts/create.html'
    # success_url = reverse_lazy('posts:post-detail-cbv')
    # login_url = "/accounts/login/"


class PostUpdateView(LoginRequiredMixin, UserOwnerMixin, UpdateView):
    queryset = Post.objects.all()
    form_class = PostModelForm
    template_name = 'posts/update.html'


class PostDeleteView(LoginRequiredMixin, DeleteView):
    model = Post
    template_name = 'posts/delete_confirm.html'
    success_url = reverse_lazy('posts:post-list')


class PostDetailView(Translate, ObjectViewedMixin, DetailView):
    queryset = Post.objects.all()
    template_name = 'posts/post_detail.html'


class PostListView(LoginRequiredMixin, ListView):
    queryset = Post.objects.all()

    def get_queryset(self, *args, **kwargs):
        request = self.request

        qs = Post.objects.all()
        query = request.GET.get("q", None)
        if query is not None:
            qs = qs.filter(
               Q(content__icontains=query) |
               Q(user__username__icontains=query)
            )
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(PostListView, self).get_context_data(*args, **kwargs)
        title = _('Homepage')
        notify = _("Notifications")
        profile = _("Profile")
        settings = _("Settings")
        search = _("Search")
        context['create_form'] = PostModelForm()
        context['create_url'] = reverse_lazy('posts:create-post')
        context['title'] = title
        context['notify'] = notify
        context['profile'] = profile
        context['settings'] = settings
        context['search'] = search

        return context


class SearchView(Translate, View):
    def get(self, request, *args, **kwargs):
        query = request.GET.get("q")
        qs = None
        if query:
            qs = User.objects.filter(
                Q(username__icontains=query)
            )
        context = {
            "users": qs
        }
        title = _('Homepage')
        notify = _("Notifications")
        profile = _("Profile")
        settings = _("Settings")
        search = _("Search")
        context['title'] = title
        context['notify'] = notify
        context['profile'] = profile
        context['settings'] = settings
        context['search'] = search
        return render(request, "posts/search.html", context)

