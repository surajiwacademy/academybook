from django.utils.timesince import timesince
from rest_framework import serializers

# from comments.api.serializers import CommentSerializer, CommentListSerializer
# from comments.models import Comment
from posts.models import Post
from accounts.api.serializers import UserDisplaySerializer


class ParentPostSerializer(serializers.ModelSerializer):
    user = UserDisplaySerializer(read_only=True)
    date_display = serializers.SerializerMethodField()
    timesince = serializers.SerializerMethodField()
    likes = serializers.SerializerMethodField()
    did_like = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = [
            'id',
            'user',
            'content',
            'timestamp',
            'date_display',
            'timesince',
            'likes',
            'did_like',

        ]

    def get_did_like(self, obj):
        request = self.context.get('request')
        user = request.user
        if user.is_authenticated:
            if user in obj.liked.all():
                return True
        return False

    def get_likes(self, obj):
        return obj.liked.all().count()

    def get_date_display(self, obj):
        return obj.timestamp.strftime("%b %d, %I:%M %p")

    def get_timesince(self, obj):
        return timesince(obj.timestamp) + "ago"


class PostSerializer(serializers.ModelSerializer):
    parent_id = serializers.CharField(write_only=True, required=False)
    user = UserDisplaySerializer(read_only=True)
    date_display = serializers.SerializerMethodField()
    timesince = serializers.SerializerMethodField()
    likes = serializers.SerializerMethodField()
    did_like = serializers.SerializerMethodField()
    parent = ParentPostSerializer(read_only=True)
    # comments = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = [
            'parent_id',
            'id',
            'user',
            'content',
            'timestamp',
            'date_display',
            'timesince',
            'parent',
            'likes',
            'did_like',
            'reply',
            # 'comments',
        ]
        # read_only_fields = ['reply']

    def get_date_display(self, obj):
        return obj.timestamp.strftime("%b %d, %I:%M %p")

    def get_timesince(self, obj):
        return timesince(obj.timestamp) + "ago"

    def get_likes(self, obj):
        return obj.liked.all().count()

    def get_did_like(self,obj):
        request = self.context.get('request')
        user = request.user
        if user.is_authenticated:
            if user in obj.liked.all():
                return True
        return False

    # def get_comments(self, obj):
    #     c_qs = Comment.objects.filter_by_instance(obj)
    #     comments = CommentListSerializer(c_qs, many=True).data
    #     return comments


