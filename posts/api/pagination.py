from rest_framework.pagination import (
    LimitOffsetPagination,
    PageNumberPagination,
    )


class StandardResultsPagination(PageNumberPagination):
    page_size = 5
    page_query_param = 'pagesize'
    max_page_size = 1000


class PostLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 10
    max_limit = 10
