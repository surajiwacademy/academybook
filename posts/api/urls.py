from django.urls import path

from .views import (PostListApiView,
                    PostCreateApiView,
                    RePostApiView,
                    LikedToggleAPIView,
                    PostDetailAPIView, SearchListApiView)

app_name = 'posts-api'

urlpatterns = [
    path('', PostListApiView.as_view(), name='list-api'),
    path('create-api/', PostCreateApiView.as_view(), name='create-api'),
    path('<int:pk>/like/', LikedToggleAPIView.as_view(), name='like'),
    path('<int:pk>/', PostDetailAPIView.as_view(), name='detail'),
    path('<int:pk>/repost/', RePostApiView.as_view(), name='repost'),

    path("search-api/", SearchListApiView.as_view(), name="search-api")


]
