
from django.urls import path
from django.views.generic.base import RedirectView

from .views import (PostDetailView,
                    PostListView,
                    PostCreateView,
                    PostUpdateView,
                    PostDeleteView,
                    RePostView, SearchView
                    )


app_name = 'posts'

urlpatterns = [
    path('', RedirectView.as_view(url='/')),
    path('create-post/', PostCreateView.as_view(), name='create-post'),
    path('post-detail-cbv/<int:pk>/', PostDetailView.as_view(), name='post-detail-cbv'),
    path('<int:pk>/repost/', RePostView.as_view(), name='repost'),
    path('post-list/', PostListView.as_view(), name='post-list'),
    path('<int:pk>/post-update/', PostUpdateView.as_view(), name='post-update'),
    path('<int:pk>/post-delete/', PostDeleteView.as_view(), name='post-delete'),
    path("search/", SearchView.as_view(), name="search"),



]
