from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden
from django.shortcuts import render
from django.utils.safestring import mark_safe
import json

from accounts.models import Profile
from .models import Message


def index(request):
    return render(request, 'chat/index.html', {})


@login_required
def room(request, profile_id):
    try:
        print(profile_id, request.user.username)
        profile = Profile.objects.get(id=profile_id)
        print(profile)
    except:
        return HttpResponseForbidden()

    return render(request, 'chat/room.html', {'profile': profile})
