import json

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
from asgiref.sync import async_to_sync
from django.utils import timezone
import aioredis

from .models import Message


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.user = self.scope['user']
        self.id = self.scope['url_route']['kwargs']['profile_id']
        self.room_group_name = 'chat_%s' % self.id
        self.r_conn = await aioredis.create_redis("redis://localhost")
        # join room group
        print(self.r_conn)
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name,
        )
        # accept connection
        await self.accept()

    async def disconnect(self, code):
        # leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # receive message from WebSocket

    async def receive(self, text_data=None, bytes_data=None):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        now = timezone.now()
        # Message.objects.create(author=self.user, content=message)
        await  self.save_message(message)
        # send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message,
                'user': self.user.username,
                'datetime': now.isoformat()
            }
        )

    # receive message from room group
    async def chat_message(self, event):
        # Send message to WebSocket

        await self.send(text_data=json.dumps(event))

    @database_sync_to_async
    def save_message(self, message):
        return Message.objects.create(author=self.user, content=message) 
