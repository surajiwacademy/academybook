
from django.urls import path, re_path
from .views import (login_view,
                    register,
                    join,
                    checkout,
                    settings,
                    activate_account,
                    logout_view,
                    UserDetailView,
                    UserFollow,
                    )
from .friendsViews import (user_list,

                           send_friend_request,
                           cancel_friend_request,
                           accept_friend_request,
                           delete_friend_request)

app_name = "profiles"

urlpatterns = [
    path('register/', register, name="register"),
    path('join/', join, name="join"),
    path('checkout/', checkout, name="checkout"),
    path('setting/', settings, name="setting"),
    path('signin/', login_view, name="login"),
    path('logout/', logout_view, name="logout"),
    re_path(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            activate_account,
            name='activate'),
    path('user-detail/<str:username>/', UserDetailView.as_view(), name="user-detail"),
    path('<str:username>/user-follow/', UserFollow.as_view(), name="user-follow"),
    path('user-list', user_list, name='user-list'),

    path('friend-request/send/<int:pk>/', send_friend_request),
    path('friend-request/cancel/<int:pk>/', cancel_friend_request),
    path('friend-request/accept/<int:pk>/', accept_friend_request),
    path('friend-request/delete/<int:pk>/', delete_friend_request),

]
