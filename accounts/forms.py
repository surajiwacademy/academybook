from django import forms
from django.contrib.auth import get_user_model, authenticate
from django.db.models import Q
from django.shortcuts import redirect

from .models import Profile

User = get_user_model()


class UserCreationForm(forms.ModelForm):
    """ Handles on creating a new user """
    email = forms.EmailField(max_length=100, help_text='Required')
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password Confirm', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'email']

    """ Validates Password """
    def clean_password(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Password Does not match!")
        return password2

    def save(self, commit=True):
        """ Saves password as well in hashing """
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])

        if commit:
            user.save()
        return user


class UserLoginForm(forms.Form):
    """ Handles for login user """
    query = forms.CharField(label='Username / Email')
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

    """ Validating user if user or password matched """
    def clean(self, *args, **kwargs):
        query = self.cleaned_data.get('query')
        password = self.cleaned_data.get('password')
        user_qs_final = User.objects.filter(
            Q(username__iexact=query) |
            Q(email__iexact=query)
        ).distinct()

        if not user_qs_final.exists() and user_qs_final.count != 1:
            raise forms.ValidationError('Invalid Credentials - User Does Not Exists')
        user_obj = user_qs_final.first()
        if not user_obj.check_password(password):
            raise forms.ValidationError("Credentials are not correct")
        if not user_obj.is_active:
            raise forms.ValidationError('User is not active')
        # if not user_obj.is_paid:
        #     raise forms.ValidationError('Please Click Membership to Access!')

        self.cleaned_data['user_obj'] = user_obj

        return super(UserLoginForm, self).clean(*args, **kwargs)


class UserUpdateForm(forms.ModelForm):
    model = User
    fields = [
        "Username",
        "email",
        "groups",
        "is_admin",
        "is_staff",
        "is_active"
    ]


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = '__all__'
