from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver, Signal
from django.utils.text import slugify

from django.conf import settings
from accounts.models import Profile

user_logged_in = Signal(providing_args=['instance', 'request'])


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_user_profile(sender, instance, created, **kwargs):
    print(created)
    if created:
        try:
            Profile.objects.create(user=instance)
        except:
            pass


@receiver(post_save, sender=Profile)
def pre_save_slug(sender, instance, *args, **kwargs):
    slug = slugify(instance.user)
    exists = Profile.objects.filter(slug=slug).exists()
    if exists:
        slug = "%s-%s" % (slug, instance.id)
        print(slug)
    instance.slug = slug
