from django.urls import path

from django.views.generic.base import RedirectView

app_name = 'profiles-api'

from posts.api.views import (PostListApiView)

urlpatterns = [
    path('<str:username>/posts/', PostListApiView.as_view(), name="list-api")
]
